/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitorpadraoprojeto;

/**
 *
 * @author Aluno
 */
public class VisitorPadraoProjeto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        br.padroes.visitor.ArvoreBinaria arvore = new br.padroes.visitor.ArvoreBinaria(7);

        arvore.inserir(15);
        arvore.inserir(10);
        arvore.inserir(5);
        arvore.inserir(2);
        arvore.inserir(1);
        arvore.inserir(20);

        System.out.println("### Exibindo em ordem ###");
        arvore.aceitarVisitante(new br.padroes.visitor.ExibirInOrderVisitor());
        System.out.println("### Exibindo pre ordem ###");
        arvore.aceitarVisitante(new br.padroes.visitor.ExibirPreOrdemVisitor());
        System.out.println("### Exibindo pós ordem ###");
        arvore.aceitarVisitante(new br.padroes.visitor.ExibirPostOrderVisitor());
        System.out.println("### Exibindo identado ###");
        arvore.aceitarVisitante(new br.padroes.visitor.ExibirIndentadoVisitor());

    }

}
